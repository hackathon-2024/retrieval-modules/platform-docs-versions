# Resource URL: https://about.linkedin.com/transparency/community-report
Community Report
================

  
This report covers actions we took on content that violated our Professional Community Policies and User Agreement for the six-month period between January 1 and June 30, 2023. It also summarizes requests to remove content based on copyright infringement claims.  

This report covers:
-------------------

[Fake accounts](#fake-accounts)

[Spam and scams](#spam-scams)

[Content violations](#content-violations)

[Copyrighted materials](#copyright-removal-requests)

Fake accounts
-------------

  
We are a safe, trusted and professional community where real people share, learn and connect. Our policies prohibit fake profiles and require that members are real people who represent themselves accurately and contribute authentically.  

### What we do to catch fake accounts

  
Our automated defenses blocked 90.1% of the fake accounts we stopped during the January - June 2023 period, with the remaining 9.9% stopped by our manual investigations and restrictions. 99.7% of the fake accounts were stopped proactively, before a member report. We continue enhancing our defenses to prevent and remove malicious accounts.  

#### Fake accounts detected and removed

2023: Jan-Jun 2022: Jul–Dec 2022: Jan–Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan-Jun 2019: Jul–Dec 2019: Jan–Jun##### 2023: January-June

            M = millions        K = thousands ##### 2022: July-December

            M = millions        K = thousands ##### 2022: January-June

            M = millions        K = thousands ##### 2021: July-December

            M = millions        K = thousands ##### 2021: January-June

            M = millions        K = thousands ##### 2020: July-December

            M = millions        K = thousands ##### 2020: January-June

            M = millions        K = thousands ##### 2019: July-December

            M = millions        K = thousands ##### 2019: January-June

            M = millions        K = thousands 

Spam and scams
--------------

  
Spam and scams are not permitted on LinkedIn. By far the most common type of inappropriate content we take action on is spam or scam content, which includes inappropriate commercial activity and repetitive communications or invitations, often meant for financial gain.

### What we do to catch spam and scams

  
Of the spam or scam content removed in this reporting period, our automated defenses stopped 99.6% and our teams manually removed the rest.  

#### Spam and scams detected and removed

2023: Jan–Jun 2022: Jul–Dec 2022: Jan - Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan–Jun 2019: Jul–Dec 2019: Jan–Jun##### 2023: January-June

M = millions        K = thousands ##### 2022: July-December

M = millions        K = thousands ##### 2022: January-June

M = millions        K = thousands ##### 2021: July-December

M = millions        K = thousands 

##### 2021: January-June

M = millions        K = thousands

_\*An earlier version of this report reflected that LinkedIn removed 232 thousand spam and scams after member reports during the reporting period of January through June 2021. We have edited the report to accurately reflect that LinkedIn removed 224 thousand spam and scams during such reporting period._

##### 2020: July-December

M = millions        K = thousands ##### 2020: January-June

M = millions        K = thousands ##### 2019: July-December

M = millions        K = thousands ##### 2019: January-June

M = millions        K = thousands 

Content violations
------------------

  
We do not tolerate any content that violates our [Professional Community Policies](https://www.linkedin.com/legal/professional-community-policies), and take swift action to remove it. We won’t always get it right, so members can [ask us to take a second look](https://www.linkedin.com/help/linkedin/answer/82934).  
  
Similar to our previous reporting periods, we continue to see record-high engagement and conversations from our growing community of over 950 million members. While we saw an overall decrease in violative content across most categories compared to the previous reporting period, we did see an increase in caught harassing and abusive content, attributable to enhanced messaging defenses.

[Learn more about content policy violations](https://www.linkedin.com/legal/professional-community-policies)

#### Content removed

2023: Jan–Jun 2022: Jul–Dec 2022: Jan-Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan-Jun 2019: Jul–Dec 2019: Jan–Jun

##### 2023: January-June

##### 2022: July-December

##### 2022: January-June

##### 2021: July-December

##### 2021: January-June

_\*An earlier version of this report reflected that LinkedIn removed 147,156 pieces of “harassment or abusive” content during the reporting period of January through June 2021. We have edited the report to accurately reflect that LinkedIn removed 158,988 pieces of content during such reporting period._

##### 2020: July-December

##### 2020: January-June

##### 2019: July-December

##### 2019: January-June

Copyright removal requests
--------------------------

  
We respect the intellectual property rights of others and do not allow copyright infringement. Our [User Agreement](https://www.linkedin.com/legal/user-agreement) requires members to respect others’ rights and follow the law, including gathering any necessary legal permissions before sharing protected content.  

2023 2022 2021 2020 2019 2018

#### Copyright removal requests

##### 2023: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 2,143 | 1,994 | 1,168 | 826 | 59% |

#### Copyright removal requests

##### 2022: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,195 | 1,214 | 1,098 | 116 | 90.44% |

#### Copyright removal requests

##### 2022: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,052 | 1,067 | 1,012 | 55  | 94.85% |

#### Copyright removal requests

##### 2021: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 872 | 881 | 863 | 18  | 97.95% |

#### Copyright removal requests

##### 2021: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,716 | 5,796 | 5,780 | 16  | 99.70% |

#### Copyright removal requests

##### 2020: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 3,876 | 23,269 | 23,260 | 9   | 99.96% |

#### Copyright removal requests

##### 2020: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 7,039 | 125,306 | 125,289 | 17  | 99.99% |

#### Copyright removal requests

##### 2019: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 11,564 | 290,170 | 290,145 | 25  | 99.99% |

#### Copyright removal requests

##### 2019: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 8,845 | 116,164 | 116,127 | 37  | 99.97% |

#### Copyright removal requests

##### 2018: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 9,333 | 115,310 | 115,284 | 26  | 100% |

#### Copyright removal requests

##### 2018: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 4,737 | 37,901 | 37,881 | 20  | 99.94% |

\[1\] Requests from copyright owners claiming infringement of protected works.

  
\[2\] Requests often cite multiple infringements.

\[3\] Infringements removed means action was taken. 

\[4\] Reported infringements not removed.